import pprint
import os, boto, boto3
import configparser
from code.JsonHelper import JsonHelper, JsonBase
from pathlib import Path
pp = pprint.PrettyPrinter(indent=4)

class EnvironmentService:

    def getAwsEksCluster(self):
        return os.getenv('AWS_EKS_CLUSTER', "test-eks-cluster")
        
class PathService:

    def getFileFromParentFolderToRoot(self, filename):
        p = Path(os.path.dirname(__file__))
        parentFolder = p.parents[0].parents[0].parents[0]
        parentFolderFile = parentFolder / filename
        parentFolderFileResolved = parentFolderFile.resolve()
        return parentFolderFileResolved
        
class CredentialsFileService:

    def _getCredentialsConfig(self):
        config = configparser.ConfigParser()

        awsCredentialsFileParentFolderFilePath = PathService().getFileFromParentFolderToRoot(filename="aws-credentials-file.txt")

        credentials_file_path = os.getenv('AWS_SHARED_CREDENTIALS_FILE', awsCredentialsFileParentFolderFilePath)
        config.read(credentials_file_path)
        return config

    def getAwsAccessKeyId(self):
        config = self._getCredentialsConfig()
        return config['default']["aws_access_key_id"]
        
    def getAwsSecretAccessKey(self):
        config = self._getCredentialsConfig()
        return config['default']["aws_secret_access_key"]
        
    def getAwsRegion(self):
        config = self._getCredentialsConfig()
        return config['default']["region"]

        
class DynamicObject(JsonBase):
    pass

class AwsService:
    def __init__(self):
        self.environmentService = EnvironmentService()
        self.credentialsFileService = CredentialsFileService()

    def getEksClient(self):
        obj = DynamicObject()
        obj.region_name = self.credentialsFileService.getAwsRegion()
        obj.aws_access_key_id = self.credentialsFileService.getAwsAccessKeyId()
        obj.aws_secret_access_key = self.credentialsFileService.getAwsSecretAccessKey()
        pp.pprint(obj.toJsonObject())

        return boto3.client(
            service_name='eks',
            region_name=obj.region_name,
            aws_access_key_id=obj.aws_access_key_id,
            aws_secret_access_key=obj.aws_secret_access_key, )

    def getEndpoint(self):
        client = self.getEksClient()
        response = client.describe_cluster(
            name=self.environmentService.getAwsEksCluster()
        )
        return response["cluster"]["endpoint"]

    def getCertificateAuthority(self):
        client = self.getEksClient()
        response = client.describe_cluster(
            name=self.environmentService.getAwsEksCluster()
        )
        return response["cluster"]["certificateAuthority"]["data"]

class KubeconfigService:
    def __init__(self):
        self.awsService = AwsService()
        self.environmentService = EnvironmentService()

    def getKubeconfig(self):
        endpoint_url = self.awsService.getEndpoint()
        base64_encoded_ca_cert = self.awsService.getCertificateAuthority()
        cluster_name = self.environmentService.getAwsEksCluster()
        return self.getKubeconfigTemplate(endpoint_url=endpoint_url, base64_encoded_ca_cert=base64_encoded_ca_cert, cluster_name=cluster_name)

    def storeKubeconfigToParentFolder(self):
        kubeconfig = self.getKubeconfig()
        print(kubeconfig)
        awsCredentialsFileParentFolderFilePath = PathService().getFileFromParentFolderToRoot(filename="kubeconfig_file.txt")
        # Todo: Open file and write to it
        with open(awsCredentialsFileParentFolderFilePath, 'w') as the_file:
            the_file.write(kubeconfig)             

    def getKubeconfigTemplate(self, endpoint_url, base64_encoded_ca_cert, cluster_name):
        template = \
"""
apiVersion: v1
clusters:
- cluster:
    server: {endpoint_url}
    certificate-authority-data: {base64_encoded_ca_cert}
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: aws
  name: aws
current-context: aws
kind: Config
preferences: {{}}
users:
- name: aws
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      command: aws-iam-authenticator
      args:
        - "token"
        - "-i"
        - "{cluster_name}"
        # - "-r"
        # - "<role-arn>"
      # env:
        # - name: AWS_PROFILE
        #   value: '<aws-profile>'
"""
        template = template.format(endpoint_url=endpoint_url, base64_encoded_ca_cert=base64_encoded_ca_cert, cluster_name=cluster_name)
        return template

if __name__ == '__main__':
    kubeconfig = KubeconfigService().getKubeconfig()
    KubeconfigService().storeKubeconfigToParentFolder()