#!/bin/sh

# If this happens:
# chmod: cannot access ‘/ubuntu/geircode_9a59f834_rsa.pub\r’: No such file or directory
# Change file in Textpad from PC to Unix.
# Or in Visual Studio. Click on the "UTF-8" bottom right button.

# Must be
# >> file 005_prepare_ubuntu_keypair.sh
# 005_prepare_ubuntu_keypair.sh: POSIX shell script, ASCII text executable

cp -r ubuntu /ubuntu
chmod 0400 /ubuntu/geircode_9a59f834_rsa.pub
chmod 0400 /ubuntu/geircode_9a59f834_rsa


