##################################################################################
# VARIABLES
##################################################################################

# variable "aws_access_key" {}
# variable "aws_secret_key" {}
# variable "private_key_path" {}

variable "shared_credentials_file" {
  default = "/var/run/secrets/aws_secrets_from_file" 
}
variable "ec2_private_key_file_path" {
  default = "/var/run/secrets/terraform_keypair_001_pem"
}
variable "ec2_public_key_name" {
  # the name of the public key pair in AWS, that is related to the private key in: "/var/run/secrets/private_key_file"
  default = "terraform-keypair-001" 
}

##################################################################################
# PROVIDERS
##################################################################################

provider "aws" {
  region                  = "eu-west-1"
  shared_credentials_file =  "${var.shared_credentials_file}"
  profile                 = "terraform"
}

##################################################################################
# RESOURCES
##################################################################################

resource "aws_instance" "nginx" {
  ami           = "ami-5bc2fb22"
  instance_type = "t2.micro"
  key_name        = "${var.ec2_public_key_name}"

  connection {
    user        = "ec2-user"
    private_key = "${file("${var.ec2_private_key_file_path}")}" 
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum install nginx -y",
      "sudo service nginx start"
    ]
  }
}

##################################################################################
# OUTPUT
##################################################################################

output "aws_instance_public_dns" {
    value = "${aws_instance.nginx.public_dns}"
}
