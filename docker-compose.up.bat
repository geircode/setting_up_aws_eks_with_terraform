REM Run Dockerfile.build.bat first if you want to create your own Container Image.
cd %~dp0
docker rm -f aws_eks_terraform-1
docker-compose -f docker-compose.yml down --remove-orphans
docker-compose -f docker-compose.yml up -d --build --remove-orphans
pause
docker exec -it aws_eks_terraform-1 /bin/bash