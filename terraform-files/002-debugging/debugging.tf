##################################################################################
# VARIABLES
##################################################################################

variable "shared_credentials_file" {
  default = "/var/run/secrets/aws_secrets_from_file" 
}
variable "ec2_private_key_file_path" {
  default = "/var/run/secrets/terraform_keypair_001_pem"
}
variable "ec2_public_key_name" {
  # the name of the public key pair in AWS, that is related to the private key in: "/var/run/secrets/terraform_keypair_001_pem"
  default = "terraform-keypair-001" 
}

##################################################################################
# PROVIDERS
##################################################################################

provider "aws" {
  region                  = "eu-west-1"
  shared_credentials_file =  "${var.shared_credentials_file}"
  profile                 = "terraform"
}

data "aws_availability_zones" "available" {}

##################################################################################
# OUTPUT
##################################################################################

# output "aws_instance_public_dns" {
#     value = "${aws_instance.nginx.public_dns}"
# }

# output "data_aws_availability_zones_available_names_1" {
#     value = "${data.aws_availability_zones.available.names[1]}"
# }

# output "data_aws_availability_zones_available_names" {
#     value = "${data.aws_availability_zones.available.names}"
# }


## Getting VPCs
# data "aws_vpcs" "get_vpc_by_tag_vpc_001_value" {
#     tags {
#       my-vpc-key = "vpc-001-value"
#   }
# }

# data "aws_vpc" "get_single_vpc_by_tag_vpc_001_value" {
#     tags {
#       my-vpc-key = "vpc-001-value"
#   }
# }

# output "001-aws_vpcs-get_vpc_by_tag_vpc_001_value" {
#   value = "${data.aws_vpcs.get_vpc_by_tag_vpc_001_value.ids}"
# }

# output "005-data.aws_vpcs.get_vpc_by_tag_vpc_001_value_ids_1" {
#   value = "${data.aws_vpcs.get_vpc_by_tag_vpc_001_value.ids[0]}"
# }

# data "aws_security_group" "my_security_group" {
#   id = "${data.aws_security_groups.my_security_groups.ids[0]}"
# }

# output "020-data.aws_security_group.my_security_group" {
#   value = "${data.aws_security_group.my_security_group.id}"
# }

# resource "aws_security_group" "default" {
#   # ...instance configuration...
# }

data "aws_vpc" "get_by_tag__vpc_001_value" {
    tags {
      my-vpc-key = "vpc-001-value"
  }
}

resource "aws_security_group" "allow_all" {
  name        = "allow_all"
  description = "Allow all inbound traffic"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1" # Type: "All traffic"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1" # Type: "All traffic"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "allow_all"
  }
}

data "aws_security_groups" "my_security_groups" {
  filter {
    name   = "vpc-id"
    values = ["${data.aws_vpc.get_by_tag__vpc_001_value.id}"]
  }
}

output "010-data.aws_security_groups.my_security_groups.ids" {
  value = "${data.aws_security_groups.my_security_groups.ids}"
}

# resource "aws_instance" "nginx2" {
#   ami           = "ami-06f42fd8c0931ce4b" # ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-20180814-3b73ef49-208f-47e1-8a6e-4ae768d8a333-ami-0b425589c7bb7663d.4
#   instance_type = "t2.nano"
#   key_name        = "${var.ec2_public_key_name}"
#   vpc_security_group_ids  = ["${data.aws_security_groups.my_security_groups.ids[0]}"]

#   connection {
#     user        = "ubuntu"
#     private_key = "${file("${var.ec2_private_key_file_path}")}" 
#   }
# }


# data "aws_security_groups" "get_by_tag__allow_all" {
#   tags {
#     Name = "allow_all",
#   }  
# }


# output "050-data.aws_security_groups.get_by_tag__allow_all" {
#   value = "${data.aws_security_groups.get_by_tag__allow_all.ids[0]}"
# }


# data "aws_security_group" "get_by_tag__allow_all" {
#   tags {
#     Name = "allow_all",
#   }  
# }

# output "060-data.aws_security_group.get_by_tag__allow_all" {
#   value = "${data.aws_security_group.get_by_tag__allow_all.id}"
# }

data "aws_subnet" "get_by_availability_zone__eu_west_1a" {
  availability_zone = "eu-west-1a"
  vpc_id = "${data.aws_vpc.get_by_tag__vpc_001_value.id}"
}

data "aws_subnet" "get_by_availability_zone__eu_west_1b" {
  availability_zone = "eu-west-1b"
  vpc_id = "${data.aws_vpc.get_by_tag__vpc_001_value.id}"
}

data "aws_subnet" "get_by_availability_zone__eu_west_1c" {
  availability_zone = "eu-west-1c"
  vpc_id = "${data.aws_vpc.get_by_tag__vpc_001_value.id}"
}

resource "aws_instance" "nginx1337" {
  ami           = "ami-06f42fd8c0931ce4b" # ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-20180814-3b73ef49-208f-47e1-8a6e-4ae768d8a333-ami-0b425589c7bb7663d.4
  instance_type = "t2.micro"
  tags {
      Name = "nginx1337"
  }
  key_name = "${var.ec2_public_key_name}"
  vpc_security_group_ids  = ["${aws_security_group.allow_all.id}"]
  subnet_id = "${data.aws_subnet.get_by_availability_zone__eu_west_1b.id}"
}


# resource "aws_instance" "nginx1337" {
#   ami           = "ami-06f42fd8c0931ce4b" # ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-20180814-3b73ef49-208f-47e1-8a6e-4ae768d8a333-ami-0b425589c7bb7663d.4
#   instance_type = "t2.micro"
#   tags {
#       Name = "nginx1337"
#   }
#   key_name = "${var.ec2_public_key_name}"
#   vpc_security_group_ids  = ["${aws_security_group.allow_all.id}"]
#   subnet_id = "${data.aws_subnet.get_by_availability_zone__eu_west_1b.id}"
# }