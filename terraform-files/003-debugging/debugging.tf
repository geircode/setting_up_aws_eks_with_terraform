##################################################################################
# VARIABLES
##################################################################################

variable "shared_credentials_file" {
  default = "/var/run/secrets/aws_secrets_from_file" 
}
variable "ec2_private_key_file_path" {
  default = "/var/run/secrets/terraform_keypair_001_pem"
}
variable "ec2_public_key_name" {
  # the name of the public key pair in AWS, that is related to the private key in: "/var/run/secrets/terraform_keypair_001_pem"
  default = "terraform-keypair-001" 
}

##################################################################################
# PROVIDERS
##################################################################################

provider "aws" {
  region                  = "eu-west-1"
  shared_credentials_file =  "${var.shared_credentials_file}"
  profile                 = "terraform"
}

# data "aws_security_groups" "get_security_group_with_tag_allow_all" {
#   tags {
#     Name = "allow_all",
#   }  
# }

# output "050-data.aws_security_groups.get_security_group_with_tag_allow_all" {
#   value = "${data.aws_security_groups.get_security_group_with_tag_allow_all.ids[0]}"
# }

data "aws_vpc" "get_vpc_by_tag__vpc_001_value" {
    tags {
      my-vpc-key = "vpc-001-value"
  }
}

output "aws_vpc_get_vpc_by_tag__vpc_001_value" {
  value = "${data.aws_vpc.get_vpc_by_tag__vpc_001_value.id}"
}

