# Commands used in "001-run-ec2-instance"

## Most important commands

```shell
ssh -i "/secrets/terraform_keypair_001_pem" ubuntu@ec2-34-245-8-253.eu-west-1.compute.amazonaws.com
ssh -i "/secrets/terraform_keypair_001_pem" ubuntu@ec2-52-213-187-252.eu-west-1.compute.amazonaws.com

terraform plan
terraform apply -auto-approve
terraform import aws_security_group.allow_all <group id ie. sg-0a830df795270dc62>
terraform import aws_instance.nginx1337 i-0c4f345a970dddfed
```
