
## Docker and Docker-Compose uses .env filen:
COMPOSE_CONVERT_WINDOWS_PATHS=1
    - makes it possible to share Sockets on Windows    
COMPOSE_PROJECT_NAME=aws_eks_terraform_9a59f834
    - overrides the default name of the compose project name so that the name of the network becomes unique


## When cloning this repository:
- create new partial GUID id => i.e. *9a59f834*-4577-4A58-8FDC-5276D6794D9E
- create new COMPOSE_PROJECT_NAME + GUID id 
- Rename "aws_eks_terraform_9a59f834" to something new.
- Run "Dockerfile.build.bat" to build the root Container for this project
- Run "docker-compose.up.bat" 


