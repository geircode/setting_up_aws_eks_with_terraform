FROM geircode/aws_eks_terraform_base:latest

WORKDIR /app
COPY . /app

RUN find . -type f -print0 | xargs -0 dos2unix

ENTRYPOINT tail -f /dev/null