# Commands used in this setup

## Most important commands

```shell
aws sts get-caller-identity
kubectl version --short --client
aws-iam-authenticator help
```

To figure out which region you are currently logged into:

```shell
aws configure get region
```

Retrieve the endpoint of the EKS cluster

```shell
aws eks describe-cluster --name test-eks-cluster --query cluster.endpoint --output text
aws eks describe-cluster --name test-eks-cluster --query cluster.certificateAuthority.data --output text
```